Tiny Cloud is a small project to allow deploying OpenStack-compatible "cloud images" for Debian and Ubuntu on to vanilla KVM based VM hosts.

This allows for KVM based VM hosting to have a more consistent environment with the cloud, as well as saving us from having to install, partition, etc new machines.

# Caveats

The error checking is not as robust as it could be. In particular supplying a build command without an IP address is not caught properly.

An image can be placed in <code>~/tinycloud/upstream/download</code>.

# How it works

The script automates the deployment of various configuration on the host (creating the LV, deploying the image in to the LV, adding the new VM in to libvirt) and within the VM (including setting up an OpenStack-compatible configdrive for cloud-init to load at boot time, setting up networking, switching back to ifupdown).

It doesn't do anything Puppet would do on its first run, though.

# Using it to build a VM

```bash
./deploy-image <path/to/image.img> <hostname> <volume group> <LV size for /> <Network bridge> 
```

```bash
root@lab-prod-kvm4:~/tinycloud# ./deploy-image download/bionic-server-cloudimg-amd64.img lab-prod-example1 vg0 10G br-vlan1003
./deploy-image: line 3: /etc/tiny-cloud: No such file or directory
*** Creating root LV for `lab-prod-example1'...
  Logical volume "vm-lab-prod-example1-root" created.
*** Writing image to root LV...
    (100.00/100%)
*** Creating VM for libvirt...
Domain lab-prod-example1 defined from /etc/libvirt/qemu/lab-prod-example1.xml

*** Creating configuration drive...
I: -input-charset not specified, using utf-8 (detected in locale settings)
Total translation table size: 0
Total rockridge attributes bytes: 763
Total directory bytes: 4458
Path table size(bytes): 40
Max brk space used 23000
189 extents written (0 MB)
Device attached successfully

*** Completed creating 'lab-prod-example1'
MAC address: 52:54:00:E4:08:86
root@lab-prod-kvm4:~/tinycloud#
```

## Cloud-init

On boot cloud-init will run what is present in tinycloud/templates/user_data

The below example:
* Configures /etc/resolv.conf
* Sets the default repository server to use
* Installs some base packages I find useful
* Creates and runs a script that puts the IPv4 addresses of the host in /etc/issue so they appear in the console
* Creates an SSH enabled user with no password and gives them the right to setup the host

You could use cloud-init to configure puppet or pretty much anything else, see: https://cloudinit.readthedocs.io/en/latest/topics/examples.html

```yaml
manage_resolv_conf: true
resolv_conf:
  nameservers: ['10.10.15.21', '10.10.15.22', '10.10.15.23']
  searchdomains:
    - i.jfnet.nz
    - jfnet.nz
  domain: i.jfnet.nz
  options:
    rotate: true
    timeout: 1
apt:
  primary:
    - arches: [default]
      uri: http://mirror.fsmg.org.nz/ubuntu/
packages:
 - pwgen
 - htop
 - iftop
 - iotop
 - sysstat
 - vim
 - byobu
 - fail2ban
 - git
 - etckeeper
 - pv
 write_files:
  - path: /set_etc-issue.sh
    permissions: 555
    content: |
      #!/bin/bash
      echo IPv4: `/sbin/ip a | grep -v link | grep -v host | grep "inet" | awk '{ print $2 }'` >> /etc/issue
runcmd:
  - /bin/bash /set_etc-issue.sh
  users:
  - name: remadmin
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups:
      - sudo
      - adm
    ssh-authorized-keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQClE9IvKhSbKzwKTUUBJv5cKEG2wtlmm4dZsYzLzciqlFZmgmS6oqV94le2hfOUtussRxpb118x5IssiTahlYCijN/EyzdTmz8WXt6g3puM+vz9hjhBEqlUtkvcCF5zKnD3tAJ9EJCqizyk15c9st8iYl7ZFTRkSj+ZlrJ7k+4JnFJxM+911DL4BxuggOHMjo7gvnoOhsxRfoAO4NkQ9Ajd222nbH8hEBhGoO3ALcA0OUOh9dipMCEIUlivJdXrMFFyGMwt7H3S1liEP/RXTenbr9cUTSyIEXjz8oxQAXp2TQS8qOUOKkWcv2Jw60Nbu7n2BWBMQl0sP1YMsF6mPsW1 remadmin@lab-prod-management1
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXkKH5LXy37P3alqampd41+dp2I6PlWyDGhYBjQJGDnySxUSxjGWFQ0kelJRItVNQ9cgTWYQVUDqAX+oaVh+09VTj8Yjw+ZwB6ACpnKkbXHKPuHI9BzmAo5Xike6nHo+A1jEJDOXURNaxsO7m0fsxV3FF/YYzFSq7GIXcZf5HcvOMkkbfRm+vxFbfJhCwxZwHbJ78L0UlomYFg7jom4e/x7f/5zrbi4k9p9hcCh6Bo24PnQ+crQ8sMa68vBR9Yet5hOyvA9i8k4+/7gp78j962y86nyPRq/jE3juHuEMtc8Y60Tukbx27/UAQfGNDm00M4xDF2hjTOxvgOoBIr2PuJ remadmin@lab-prod-management2
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHip6J4MbCjiD8DhZbBuHDx9xOPYK22bVC+BTg134g8SjRRQOls989+bnhvb/HFgeP3fTmyhI1gH3XHw1E4HzgkRxvHCDJkvJdIAOU3JkcvAgfHWLjTas+j10cAG5LnTp1/P8mELFjemgMnMyC5x9TXAyhlZewD76SQcp1KpNjbnc8Bhod1yo+KHHTjzfa4sBwR4dhi/XQ9Q28/lrZUYDEB21FNHPrs8cSb4UtUZtjKBSR1DopWIBXhFx/KQODXN4rM3gqi5AjTirjUgzwdBqXuBiTCOeo1RxXiiLhguYMG+Q8TuPhyAAOJHyNVTEWn7HHyTX4oA0h8e5PmCHJRI5r remadmin@lab-prod-management3
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxfQh9ycdaCISiRO+GxM13ssdCFnZ0jJmgoFsBUGMJPpMh0XkRYRRYBsdtIge26WChbTjnfa1jHkclabAxhzaX74xyqjx3bFOTT/Gh2khgKexs9F69Mk+hbSGvgAVlXp7Ms/Y9gPlA0vdexxjzAqh6xIEDt/FurehY+tdUS4DS2t9+DPjWJGZg/G5YU2Q/9YNB5maN7UorFKRGAZbdWhN6NmT/X2G+6nZ2LeRa+EZ2lQyUy+6m6ZE+wj0k8BxxnBEeNVtFp9j2birV+KawN1IOIxE7eKkP76GcWZGDD+ctVjCsyKNPvFDGR14mOXwQf1biQnSjuhthcKJi4+rw9Dxb james@jmba-lubuvm
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVGjIzaBDqI9/Df7A4ot4Ys/WLQ9SiTDLpbpmmwd2TyrRDMs26oBO2YEHiL1borLBDQEFVGIcs6XoOIIWeCpnwvkl3w11dYQrX4yOZgeuSF1kUWHgnBnvgJXcofZK/HsPHZC1ybZ7SoEtc7yiylgoYGwzQ//aBGT2KptMUeh0c6Jr5owEtNRhMtPNNFV1j1CfG7osR2pmWQ4BfIMk47X70mjiS+B+YOJ6nbnTcwBdUQ3uVvabZt6bttJJTxbqHdWlWKTRuOleGRXRbLElvcOdqU26NY2J2d68hrK272D6kKKhHESRj2F0N2cPlVWdPzqe2tXMglKgX5DKUHrQNWv2B james@james-desktopubu
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7MktT5vXs2HPFpj5cV8MXbfoR/+B2ic3xD1FmjQITs0XfZPCy+4wn/j54gteY/4xHxBReGQQxUqFnaJtVX8pW0pqaA+Qva6/JsAswz6sOHKetLZESDvAc1OourOkONJP44Z/MfwFI7VxePqhrojfPPguXyCmr88qpS26wYO3kB0GlwTZG41ECIVQbdMjtpj0bu13UOikLiugCIWYtcKRF1CAdpcQHyuqPRlDplkN+5E+R3oXKZ5ZwyKYPyDGKRkOXelMrgNtoUl1IM1a6d6rd53A/Ylh1K1asvtycNw01EH59KUwil5STDYoPhOf0HuV9TnvvBk7rVKJt6/Pk0BPb james@JMBA.local
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgMQXf4swtx73t56QXS8CO0hcXejEnUofKx+q0Px/+WN3foJasfTHjDm+zqDO2vab7KzEsury0m4UWQAymQonfFXvgltmWDdxW0aydhyBTfGl/l3Vohsosk/43zwLHcvZKA+2+iWk2vKe3csynjaQoXWO8fpKqZLsGYu89avi72Gw7s9oc+BcKvq+iLdXsrjTv5duTiQIx0au+nv5OH5S7QCuyUTkt1xjQn+hRR+Pjl1VHhIs8V/HttY/B7bbdBHbSls9hTCsUoT7ecz586JxKYAnahchqRCudHXdP2WgOfhoUOn+Ui9WMYr+JgLY6uczrkKbF1RSRTKkjSolvw/YF james@j-u-desktop
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsRASW63egge4WXbWlwT4x40q64siZ0Em5AnwmQ1GbmrNEOJCBPceDjonlIZI6kDYYzBa16Jgs/YlW+vjGflyGaB6FHGhMsdsKcavDCnDS6TspuCnaL7SltaR9XPbEZ0IarJ8H/nsw4LhsLgaZ8KUtjfcA3zlUjBBnwYB1QNQ3UljoGBhZI+krtNEiNQkNkrv3lrfKomZ1dEJff98cfrSiBafyp/gCEDn47L5f8UQXk5DNfCNHC/1gtfEJqGFf7UFMOZiAPy1fd60kfLue10kd0/FWTBviSu49vrfPXq9adio197uSSDSHTJAyggcXfHd5Uy9H4DpQ0wKaXE4dR/8f james@jmba-udvm
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC1aR1In5IGr4RpYk0rW3pcWAut8boQZJCn2GFT6oTYWufjI660+Ur9ZcsVEf7i/rTXh4+lAg+T0MNe+iIu1mBg4lpZSUgcD9T3j04FMhCYNPeWJ+ya77i2W87GGI/LJfzIKVOvsgY4NWOQOx/d3qXlfG4MYdqlyU8MFIfYu+yxOumfxvODV+9CPtlk15xupZHeazuHhwHkjjS2nuElLAbdQAMeMU87tlaaF/NMVXIJjs3eABYvrkyYLg1Vj5xtYWyu/51u5VzzRU0oC85IzYnFV5lGZwJ/+bri7UhPhI8xx2TM1hl0vmj4R+5Mb+snfrc50i+5s3nijGSkIdX8Q2MYb0QksRwAgZJ6ZNTCGtqXhniBPNhnbyX+/Jiv8/5tkk4xtX1fK0VvQN0FLVUTryI0P2F7/5o8rShOlB6KqQ9nGWEdPjBLnXkSNu3n9Qc66tHOtcrZCa2LxRzSIJ6kb7rdduxw5jXcOkZeUNlCtkdVXI49ldBKO7s3eJ7CnmQGfKhj5MBY2krgAI6o/T18HEfjlgZGVCVKz/vG+vX+4FZ/xnDxBywAcQ5PJoPGS4y6Mrog8jWPthiMRDc5h7WHZW+wavpgr74h00rGw1dbS2rbEL4KJNFYxPnKT+/+OKIowcj7QIZmJzm4sZ4JQZ4C9a77BMSurHr8csJqowPxsGMcbw== jforman@work-desktop
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDP+KrA4HOQ2Fd8fj9tnCzshNDNtY8KmmapAxdh3p3/oE27iy2E/7hAHSCbT4yAKwGU0UgiUDmT1qcyxXhfblEZGyRdHnq9EZX/t95BEQHIBO/NZ+o6KjDJWJG9m29ALkv1ayjxJS1van9KXIhHseLl1WLvMtuejVFZjGVtn8uZsrgZLA9CZVUwFk5TwsRCsIoyoJZUSKTUEJYIRNsKm8OJp89SbBvy6MLT+dkou9FmAx3QZZ/1EwvKZRCXD5VbxKgrmGzAvnhDqW7/+FYEyABUaSmI3L2czrPOTh14/0QtKSErE5Z3lj2/qBtpwP5od3VyuLCR27EwGdrlmu9FgO6x jforman@work-laptop
```